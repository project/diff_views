Diff Views
==========

Drupal Views handler field **Compare** with Diff radiobuttons.

1. Add new “Compare” field to Revision View.
2. Select 2 revisions of the same node and click “Compare” button.
3. You will be redirected on Diff page `node/%/revisions/view/%/%`.

Looks better in *Table* format.

If you would like to add rows highlighting from Diff module,
add CSS class 'diff-views-rows-js' to your view
("Advanced" → "Other" → "CSS class" in Views).
